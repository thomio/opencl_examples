### Example from:

 - https://anteru.net/blog/2012/11/03/2009/index.html
 - https://bitbucket.org/Anteru/opencltutorial

### INSTALL:
 - mkdir build && cd build
 - cmake .. && make
 - make install
