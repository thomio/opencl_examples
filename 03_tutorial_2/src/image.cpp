#include <iostream>
#include <vector>
#include <string>
#include <cstdlib>
#include <fstream>
#include <sstream>
#ifdef __APPLE__
  #include "OpenCL/opencl.h"
#else
  #include "CL/cl.h"
#endif


std::string GetPlatformName(cl_platform_id id)
{
  size_t size = 0;
  clGetPlatformInfo(id, CL_PLATFORM_NAME, 0, NULL, &size);

  std::string result;
  result.resize(size);
  clGetPlatformInfo(id, CL_PLATFORM_NAME, size,
    const_cast<char*>(result.data()), NULL);

  return result;
}

std::string GetDeviceName(cl_device_id id)
{
  size_t size = 0;
  clGetDeviceInfo(id, CL_DEVICE_NAME, 0, NULL, &size);

  std::string result;
  result.resize(size);
  clGetDeviceInfo(id, CL_DEVICE_NAME, size,
    const_cast<char*>(result.data()), NULL);

  return result;
}

void CheckError(cl_int error)
{
  if(error != CL_SUCCESS){
    std::cerr << "OpenCL call failed with error: " << error << std::endl;
    std::exit(1);
  }
}

std::string LoadKernel(const char* name)
{
  std::ifstream in(name);
  std::string result(
      (std::istreambuf_iterator<char>(in)),
      std::istreambuf_iterator<char>());

  return result;
}

cl_program CreateProgram(const std::string &source, cl_context context)
{
  size_t length[] = {source.size()};
  const char* source_ptr[] = {source.c_str()};
  //const std::vector<char> source_ptr(source.c_str(), source.c_str() + source.size()+1);

  cl_int error = 0;
  cl_program program = clCreateProgramWithSource( context, 1, source_ptr, length, &error );
  CheckError(error);

  return program;
}

struct Image
{
  std::vector<char> pixel;
  int width, height;
};

Image LoadImage (const char* path)
{
  std::ifstream in (path, std::ios::binary);
  std::string s;
  in >> s;

  if (s != "P6") {
    exit (1);
  }
  // Skip comments
  for (;;) {
    getline (in, s);
    if (s.empty ()) {
	continue;
    }
    if (s [0] != '#') {
	break;
    }
  }
  std::stringstream str (s);
  int width, height, maxColor;
  str >> width >> height;
  in >> maxColor;
  if (maxColor != 255) {
    exit (1);
  }

  {
    // Skip until end of line
    std::string tmp;
    getline(in, tmp);
  }

  std::vector<char> data (width * height * 3);
  in.read (reinterpret_cast<char*> (data.data ()), data.size ());

  const Image img = { data, width, height };
  return img;
}

void SaveImage (const Image& img, const char* path)
{
  std::ofstream out (path, std::ios::binary);
  out << "P6\n";
  out << img.width << " " << img.height << "\n";
  out << "255\n";
  out.write (img.pixel.data (), img.pixel.size ());
}

Image RGBtoRGBA (const Image& input)
{
  Image result;
  result.width = input.width;
  result.height = input.height;

  for (std::size_t i = 0; i < input.pixel.size (); i += 3) {
    result.pixel.push_back (input.pixel [i + 0]);
    result.pixel.push_back (input.pixel [i + 1]);
    result.pixel.push_back (input.pixel [i + 2]);
    result.pixel.push_back (0);
  }

  return result;
}

Image RGBAtoRGB (const Image& input)
{
  Image result;
  result.width = input.width;
  result.height = input.height;

  for (std::size_t i = 0; i < input.pixel.size (); i += 4) {
    result.pixel.push_back (input.pixel [i + 0]);
    result.pixel.push_back (input.pixel [i + 1]);
    result.pixel.push_back (input.pixel [i + 2]);
  }

  return result;
}

int main(){

  // Search for platforms:
  cl_uint platformIdCount = 0;
  clGetPlatformIDs(0, NULL, &platformIdCount);

  if(platformIdCount == 0){
    std::cerr << "No OpenCL platform found" << std::endl;
    std::exit(1);
  }else{
    std::cout << "Found " << platformIdCount << " platform(s)" << std::endl;
  }

  std::vector<cl_platform_id> platformIds(platformIdCount);
  clGetPlatformIDs(platformIdCount, platformIds.data(), NULL);

  for(cl_int i = 0; i < platformIdCount; ++i){
    std::cout << "\t (" << (i+1) << ") : " << GetPlatformName(platformIds[i]) << std::endl;
  }

  // Search devices:
  cl_uint deviceIdCount = 0;
  clGetDeviceIDs(platformIds[0], CL_DEVICE_TYPE_ALL, 0, NULL, &deviceIdCount);
  if(deviceIdCount == 0){
    std::cerr << "No OpenCL devices found" << std::endl;
    return 1;
  }else{
    std::cout << "Found " << deviceIdCount << " device(s)" << std::endl;
  }

  std::vector<cl_device_id> deviceIds(deviceIdCount);
  clGetDeviceIDs(platformIds[0], CL_DEVICE_TYPE_ALL, deviceIdCount, deviceIds.data(), NULL);

  for(cl_int i = 0; i < deviceIdCount; ++i){
    std::cout << "\t (" << (i+1) << ") : " << GetDeviceName(deviceIds[i]) << std::endl;
  }


  const cl_context_properties contextProperties[] =
  {
    CL_CONTEXT_PLATFORM,
    reinterpret_cast<cl_context_properties>(platformIds[0]),
    0, 0
  };

  cl_int error = CL_SUCCESS;
  cl_context context = clCreateContext(
    contextProperties,
    deviceIdCount,
    deviceIds.data(),
    NULL,
    NULL,
    &error
  );
  CheckError(error);
  std::cout << "Context created." << std::endl;

  // Gaussian blur filter
  float filter [] = {
    1, 2, 1,
    2, 4, 2,
    1, 2, 1
  };
  // Normalize the filter
  for(int i = 0; i < 9; ++i){
    filter[i] /= 16.0f;
  }

  cl_program program = CreateProgram(LoadKernel("kernels/image.cl"), context);
  CheckError(
    clBuildProgram(
      program,
      deviceIdCount,
      deviceIds.data(),
      "-D FILTER_SIZE=1",
      NULL,
      NULL)
  );

  cl_kernel kernel = clCreateKernel(program, "FILTER", &error);
  CheckError(error);

  // OpenCL only supports RGBA, we need to convert
  const char path_to_image[] = {"./image/test.ppm"};
  std::cout << "Reading image from: " << path_to_image << std::endl;
  const auto image = RGBtoRGBA(LoadImage(path_to_image));

  static const cl_image_format format = {CL_RGBA, CL_UNORM_INT8 };
  cl_mem inputImage = clCreateImage2D(
      context,
      CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, 
      &format, 
      image.width,
      image.height,
      0,
      const_cast<char*>(image.pixel.data()),
      &error);
  CheckError(error);

  cl_mem outputImage = clCreateImage2D(
      context,
      CL_MEM_WRITE_ONLY,
      &format, 
      image.width,
      image.height,
      0,
      NULL,
      &error);
  CheckError(error);

  cl_mem filterWeightsBuffer = clCreateBuffer(
      context,
      CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
      sizeof(float) * 9, filter, &error);
  CheckError(error);

  // Setup the kernel argument
  clSetKernelArg(kernel, 0, sizeof(cl_mem), &inputImage);
  clSetKernelArg(kernel, 1, sizeof(cl_mem), &filterWeightsBuffer);
  clSetKernelArg(kernel, 2, sizeof(cl_mem), &outputImage);

  // clCreateCommandQueue is deprecated in OpenCL 2.0
    cl_command_queue queue = clCreateCommandQueue(
//  cl_command_queue queue = clCreateCommandQueueWithProperties(
    context,
    deviceIds[0],
    0,
    &error
  );
  CheckError(error);

  // Run the processing
  std::size_t offset[3] = {0};
  std::size_t size[3] = {image.width, image.height, 1};
  CheckError( clEnqueueNDRangeKernel(queue, kernel, 2, offset, size, NULL,
      0, NULL, NULL));

  // Prepare the result image, set to black
  Image result = image;
  std::fill(result.pixel.begin(), result.pixel.end(), 0);

  // Get the result back to the host
  std::size_t origin[3] = {0};
  std::size_t region[3] = {result.width, result.height, 1};
  clEnqueueReadImage(queue, outputImage, CL_TRUE, origin, region, 0, 0,
    result.pixel.data(), 0, NULL, NULL);

  SaveImage(RGBAtoRGB(result), "output.ppm");

  clReleaseMemObject(outputImage);
  clReleaseMemObject(filterWeightsBuffer);
  clReleaseMemObject(inputImage);
  clReleaseCommandQueue(queue);
  clReleaseKernel(kernel);
  clReleaseProgram(program);
  clReleaseContext(context);

  return 0;
}
